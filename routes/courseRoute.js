const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
// router.post("/create", (req, res) => {
//   courseController
//     .addCourse(req.body)
//     .then((resultFromController) => res.send(resultFromController));
// });

// my solution s39 activity
// router.post("/create", auth.verify, (req, res) => {
//   const userData = auth.decode(req.headers.authorization);
//   console.log(userData);

//   if (userData.isAdmin == false) {
//     res.send("Can't create course. Not an admin");
//   } else {
//     courseController
//       .addCourse(req.body)
//       .then((resultFromController) => res.send(resultFromController));
//   }
// });

//maam riza's
router.post("/create", auth.verify, (req, res) => {
  const data = {
    course: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  courseController
    .addCourse(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for retrieving all the courses
router.get("/all", (req, res) => {
  courseController
    .getAllCourses()
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/active", (req, res) => {
  courseController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController));
});

router.get("/:courseId", (req, res) => {
  courseController
    .getCourse(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

router.put("/:courseId", (req, res) => {
  courseController
    .updateCourse(req.params, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

router.patch("/:courseId/archive", auth.verify, (req, res) => {
  courseController
    .archiveCourse(req.params, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
